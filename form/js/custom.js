// const url =
//   "https://script.google.com/macros/s/AKfycbwl7mxPnS418m3pgPhe0TzvwFUkg-uaWkV5fFJVo-nEEdaA9xobSrzV-lbJSu35XCLX/exec";

// const url =
//   "https://script.google.com/macros/s/AKfycbxZnT7Itl9n9rQboaCPjMNeK5d4BXMWWYgwojZdZLvEU43DmIylZzVT3xRV4C5VU627iA/exec";

const url = 
    "https://script.google.com/macros/s/AKfycbyhANxq6rVaKR3i-cBjqVSTUOHMXQFOb0inMBpFvQLhWdrzuS6GzgRhjJ7_kPGuaT7vgQ/exec"

let data = [];

async function getData() {
  await fetch(url).then((v) => v.json().then((j) => (data = j)));
  $(document).ready(function () {
    $("#rekomendasi_panitia").select2({
      data: data,
    });
  });
  $(document).ready(function () {
    $("#nama").select2({
      data: data,
    });
  });

  document.getElementById("load").remove();
  console.log("hei");
  console.log(data);
}

// async function submitForm(e) {
//   e.preventDefault();

//   let nama = document.getElementById("nama");
//   let persetujuan = document.getElementById("persetujuan");
//   let moment = document.getElementById("moment");
//   let kepanitiaan = document.getElementById("kepanitiaan");
//   let rekomendasi_panitia = document.getElementById("rekomendasi_panitia");
//   let message = document.getElementById("message");

//   let data = {
//     nama: nama,
//     persetujuan: persetujuan,
//     moment: moment,
//     kepanitiaan: kepanitiaan,
//     rekomendasi_panitia: rekomendasi_panitia,
//     message: message,
//   };

//   let res = await fetch(url, {
//     method: "POST",
//     headers: {
//       Accept: "application/json",
//       "Content-Type": "application/json",
//     },
//     body: data,
//   });

//   await res.json(d => console.log(d))
// }

async function submitForm(e) {
  const form = document.querySelector("#form-survei");
  const submitButton = document.querySelector("#submit");
  submitButton.disabled = true;
  e.preventDefault();
  
  let requestBody = new FormData(form);
  fetch(url, { method: "POST", body: requestBody })
    .then((response) => {
      alert("Success!", response);
      submitButton.disabled = false;
    })
    .catch((error) => {
      alert("Error!", error.message);
      submitButton.disabled = false;
    });
}

const form = document.getElementById("form-survei");
form.addEventListener("submit", (e) => submitForm(e));

window.onload = () => {
  getData();
};
